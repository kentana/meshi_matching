require 'spec_helper'

describe User do
  # userオブジェクトを生成
  before do
    @user = User.new(
      name: "kentana",
      password: "kentana",
      password_confirmation: "kentana",
      favor: "らーめん",
      hometown: "駒沢大学",
      mail: "kentana.jp@gmail.com"
    )
  end

  subject { @user }

  it { should respond_to :name}
  it { should respond_to :password}
  it { should respond_to :password_confirmation}
  it { should respond_to :favor}
  it { should respond_to :hometown}
  it { should respond_to :mail}
  it { should respond_to :remember_token}
  it { should respond_to :authenticate}
  it { should be_valid }

  # バリデーション
  # 必須項目のテスト
  %w( name mail ).each do |column|
    describe "#{column}は空にしてはダメ" do
      before do
        # 空文字列を列の値に設定
        @user[column] = ""
      end

      it "バリデーションで弾く" do
        expect(@user).not_to be_valid
        expect(@user.errors[column]).to be_present
      end
    end
  end


  describe "nameの長さでバリデーション" do
    before do
      @user.name = "a" * 50
    end

    it { should_not be_valid }
  end


  describe "メールアドレスのバリデーション" do
    it "不正なアドレスのときに弾く" do
      %w( hoge hoge@foo,com hoge@hoge. hoge@hoge_hoge.com hoge@hoge+hoge.com ).each do |invalid_address|
        @user.mail = invalid_address
        expect(@user).not_to be_valid
      end
    end

    it "正しいアドレスのときに弾かない" do
      %w( hoge@hoge.COM h_o_g_e@hoge.com hoge.hoge@hoge.hoge hoge+hoge@hoge.hoge).each do |valid_address|
        @user.mail = valid_address
        expect(@user).to be_valid
      end
    end

    describe "アドレスは重複させない" do
      before do
        same_mail = @user.dup
        same_mail.save
      end

      it { should_not be_valid }
    end
  end

  describe "パスワードのバリデーション" do
    describe "パスワードが6文字未満なら弾く" do
      before do
        @user.password = "hoge"
      end

      it { should_not be_valid }
    end

    describe "パスワードが確認と一致しないときは弾く" do
      before do
        @user = User.new(
          name: "kentana",
          mail: "kentana.jp@gmail.com",
          password: "test",
          password_confirmation: "hoge",
        )
      end

      it { should_not be_valid }
    end
  end

  describe "authenticateメソッドのテスト" do
    before { @user.save }
    let(:found_user) { User.find_by(mail: @user.mail)}

    describe "正しいパスワード" do
      it { should eq found_user.authenticate(@user.password) }
    end

    describe "不正なパスワード" do
      let(:user_for_invalid_password) { found_user.authenticate("invalid") }

      it { should_not eq user_for_invalid_password }
      specify { expect(user_for_invalid_password).to be_false }
    end
  end

  describe "remember_tokenのテスト" do
    before { @user.save }
    its(:remember_token) { should_not be_blank }
  end
end
