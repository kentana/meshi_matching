require 'spec_helper'

describe UsersController do

  # visitメソッドのテスト
  # describe "visitの確認" do
  #   it "homeページ" do
  #     visit home_path
  #     expect(page).to have_title("Home")
  #   end

  #   it "newページ" do
  #     visit new_path
  #     expect(page).to have_title("登録")
  #   end

  #   it "resultページ" do
  #     visit result_path
  #     expect(page).to have_title("結果")
  #   end
  # end

  describe "GET 'result'" do
    it "returns http success" do
      get 'result'
      response.should be_success
    end
  end

  describe "GET 'home'" do
    it "returns http success" do
      get 'home'
      response.should be_success
    end
  end

  describe "GET 'new'" do
    it "returns http success" do
      get 'new'
      response.should be_success
    end
  end

  # describe "ユーザー登録のテスト" do
  #   subject { page }
  #   before { visit new_path }

  #   let(:submit) { "登録" }

  #   describe "不正な入力の時に弾く" do
  #     it "全部がからの時に弾く" do
  #       expect { click_link submit }.not_to change(User, :count)
  #     end
  #   end

  #   describe "正しい入力の時に登録する" do
  #     before do
  #       fill_in "name", with: "testuser"
  #       fill_in "mail", with: "test@gmail.com"
  #       fill_in "password", with: "testtest"
  #       fill_in "password_confirmation", with: "testtest"
  #     end

  #     it "ユーザーが登録される" do
  #       expect { click_link submit }.to change(User, :count).by(1)
  #     end
  #   end
  # end
end
