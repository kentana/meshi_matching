# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$ ->
  # 最初のinputにフォーカスを当てる
  $($("input")[0]).focus()


  # ぐるなびapiのアクセスキーとURL
  apiKey = "1ad60d55eec352f83695269448df174e"
  apiSearchURL = "http://api.gnavi.co.jp/ver1/RestSearchAPI/?"
  sendURL = apiSearchURL + "keyid=" + apiKey

  # 検索ボックスのDOMを取得
  searchBoxes = $(".search-box")

  # レスポンスのXMLの中身をタグ名から取得
  getXMLContent = (xml, tagName) ->
    xml.getElementsByTagName(tagName)[0].innerHTML

  # NodeListを配列に変換
  convertToArray = (nodeList) ->
    ary = []
    $.each(nodeList, (i) -> ary.push(nodeList[i]))
    ary

  # 検索ボタンをおした時にぐるなび検索する
  $("#search-button").on "click", ->
    #2つの検索窓の値をフリーワードとして文字列化
    freeWord = searchBoxes.map((i) -> searchBoxes[i].value)
    freeWordStr = freeWord.toArray().join(",")

    # 検索結果の取得を20件にする
    hitPerPage = 20

    # apiにクエリを投げる
    $.get sendURL, {freeword: freeWordStr, offset: randomOffset, hit_per_page: hitPerPage}, (shopData) ->
      # 返ってきたXMLから、店のデータを抽出して、cookieにセット
      shopsNodeList = shopData.getElementsByTagName("rest")
      $.cookie "shopXML", shopsNodeList

      # 店のデータを配列にしてcookieにセット
      $.cookie "shopData", convertToArray(shopsNodeList)

      # 検索する値を渡しつつ、resultページに遷移する
      location.href = "/users/result"


  # resultページだった場合、cookieから内容を書き込む
  if location.href.indexOf("result") > 0
    # XMLをcookieから取得
    shops = $.cookie "shopData"

    # 取得する店のインデックスをランダムで取得
    shopIndex = Math.floor(Math.random() * shops.length)

    # リコメンドする店を選ぶ
    recommend = shops[shopIndex]

    # リコメンドした店をcookieから消す
    shops.splice shopIndex, 1
    $.cookie "shopData", shops

    # 必要なデータを取る
    shopName = getXMLContent recommend, "name"
    shopImage = getXMLContent recommend, "shop_image1"
    shopDescription = getXMLContent recommend, "pr_long"
    shopOpenTime = getXMLContent recommend, "opentime"
    shopTel = getXMLContent recommend, "tel"
    shopAddress = getXMLContent recommend, "address"
    shopLine = getXMLContent recommend, "line"
    shopStation = getXMLContent recommend, "station"
    shopStationExit = getXMLContent recommend, "station_exit"
    shopWalk = getXMLContent recommend, "walk"

    # アクセスを作る
    shopAccess = shopLine + shopStation + shopStationExit + "より" + shopWalk + "分"

    # リコメンドを使い果たしたらなんかしたい
    if shops.length is 0
      # cookieの配列をリセット
      $.cookie "shopData", convertToArray($.cookie("shopXML"))

      alert $.cookie("shopData").lenth + "回も聞き返さなくても…"
