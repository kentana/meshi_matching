class SessionsController < ApplicationController

  def new
  end

  def create
    # 入力されたユーザー名からUserモデルを検索
    user = User.find_by_name(params[:session][:name].downcase)

    if user and user.authenticate(params[:session][:password])
      # ユーザーをサインインして、検索ページに飛ぶ
      sign_in user
      redirect_to user
    else
      # エラーメッセージ出して、ページ遷移なし
      flash.now[:danger] = "ユーザー名またはパスワードが正しくありません"
      render 'new'
    end
  end

  def destroy
    sign_out
    redirect_to root_url
  end
end
