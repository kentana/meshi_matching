class UsersController < ApplicationController
  before_action :signed_in_user, only: [:edit, :update]
  before_action :correct_user, only: [:edit, :update]

  # 検索結果表示
  def result
  end

  # ホーム
  def home
  end

  # 各ユーザーのデータを表示
  def show
    @user = User.find_by_id(params[:id])
  end

  # ユーザーの新規作成
  def new
    @user = User.new
  end

  # ユーザー情報の編集
  def edit
    # @user = User.find_by_id(params[:id])
    @user = current_user
  end

  # edit時の情報を保存
  def update
    @user = current_user

    if @user.update_attributes(user_params)
      # 更新成功時
      flash.now[:success] = "ユーザー情報を変更しました"
      redirect_to @user
    else
      # 更新失敗時
      render 'edit'
    end
  end

  # 登録ボタン時のアクション
  def create
    @user = User.new(user_params)

    if @user.save
      # 保存成功時
      sign_in @user
      flash.now[:success] = "登録ありがとうございます。"
      redirect_to @user
    else
      # 保存失敗時
      render 'new'
    end
  end

  private

  def user_params
    params.require(:user).permit(:name, :mail, :password, :password_confirmation)
  end

  # ログインしてないとeditとupdateをさせない
  def signed_in_user
    unless signed_in?
      flash[:danger] = "ログインしてください"
      redirect_to signin_url
    end
  end

  # ログインユーザーが正しいか判定
  def correct_user
    @user = User.find_by_id(params[:id])
    redirect_to(root_path) unless current_user?(@user)
  end
end
