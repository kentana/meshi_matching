class User < ActiveRecord::Base
  before_save do
    self.name = name.downcase
    self.mail = mail.downcase
  end

  before_create :create_remember_token

  validates :name, length: { maximum: 30 }, uniqueness: true

  # チェック用変数
  description = {
    name: "ユーザー名",
    mail: "メールアドレス",
    password: "パスワード",
    password_confirmation: "確認用パスワード"
  }

  name_min_length = 6
  name_max_length = 30
  password_min_length = 6
  password_max_length = 30

  # 必須チェック
  required_columns = [:name, :mail, :password, :password_confirmation]
  required_columns.each do |column|
    validates_presence_of column, message: "#{description[column]}を入力してください。"
  end

  # ユニークチェック
  unique_columns = [:name, :mail]
  unique_columns.each do |column|
    validates_uniqueness_of column, message: "#{description[column]}はすでに使われています。", case_sensitive: false
  end

  # 長さチェック
  length_columns = {
    name: {min: name_min_length, max: name_max_length},
    password: {min: password_min_length, max: password_max_length},
  }

  length_columns.each do |column, length|
    validates_length_of column, minimum: length[:min], message: "#{description[column]}は最低#{length[:min]}文字以上必要です。"
    validates_length_of column, maximum: length[:max], message: "#{description[column]}は最大#{length[:max]}文字です。"
  end

  # メアドの体裁チェック
  VALID_MAIL_ADDRESS = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates_format_of :mail, with: VALID_MAIL_ADDRESS, message: "不正なメールアドレスです。"


  # 安全なパスワードを使う
  has_secure_password


  # remember_tokenを作る
  def User.new_remember_token
    SecureRandom.urlsafe_base64
  end

  # tokenを暗号化
  def User.encrypt(token)
    Digest::SHA1.hexdigest(token.to_s)
  end

  private

  # remember_tokenを作成
  def create_remember_token
    self.remember_token = User.encrypt(User.new_remember_token)
  end
end
