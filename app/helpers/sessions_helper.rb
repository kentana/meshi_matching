module SessionsHelper

  # サインインする
  def sign_in(user)
    remember_token = User.new_remember_token
    cookies.permanent[:remember_token] = remember_token
    user.update_attribute(:remember_token, User.encrypt(remember_token))
    self.current_user = user
  end

  # サインアウトする
  def sign_out
    self.current_user = nil
    cookies.delete(:remember_token)
  end

  # session内のユーザーを取得
  def current_user=(user)
    @current_user = user
  end

  # ログイン中のユーザーか判定
  def current_user?(user)
    user == current_user
  end

  # sessionにユーザーをセット
  def current_user
    remember_token = User.encrypt(cookies[:remember_token])
    @current_user ||= User.find_by_remember_token remember_token
  end

  # サインイン判定
  def signed_in?
    !current_user.nil?
  end
end
