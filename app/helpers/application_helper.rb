module ApplicationHelper
  # ユーザーがログインしてるかを返す
  def login? user
    return user.login if user
    false
  end
end
