class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name
      t.string :password
      t.string :favor
      t.string :hometown
      t.string :mail
      t.boolean :login, :default => false

      t.timestamps
    end
  end
end
